import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { IProduct } from '../product';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';

@Injectable()
export class ProductService {
  private _productsUrl = './assets/api/products/products.json';
  constructor(private _http: HttpClient) {}
  getProducts(): Observable<IProduct[]> {
    return (
      this._http
        .get<IProduct[]>(this._productsUrl)
        // .do(data => console.log('All:', JSON.stringify(data)))
        .catch(this.handleError)
    );
  }
  // getProduct(id: number) {
  //   let product = {};
  //   this.getProducts().subscribe()
  //     products => {
  //       product = products.find(p => p.productId === id);
  //     });
  // }

  handleError(err: HttpErrorResponse) {
    console.log(err.message);
    return Observable.throw(err.message);
  }
}
