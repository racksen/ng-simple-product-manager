import { NgModule } from '@angular/core';
import { RouterModule } from "@angular/router";
import { ProductListComponent } from "./product-list";
import { ProductDetailComponent } from "./product-detail";
import { ProductService, ProductGaurdService } from "./services";

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: "products", component: ProductListComponent },
      {
        path: "products/:id",
        canActivate: [ProductGaurdService],
        component: ProductDetailComponent
      }
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class ProductRoutingModule {}
