import { NgModule } from '@angular/core';
import { SharedModule } from '../shared';
import { ProductRoutingModule } from './product-routing.module';
import { ProductListComponent } from './product-list';
import { ProductDetailComponent } from './product-detail';
import { ProductService, ProductGaurdService } from './services';

@NgModule({
  imports: [SharedModule, ProductRoutingModule],
  declarations: [ProductListComponent, ProductDetailComponent],
  providers: [ProductGaurdService, ProductService]
})
export class ProductModule {}
