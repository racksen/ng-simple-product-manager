import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { IProduct } from '../product';
import { ProductService } from '../services';
import { Observable } from 'rxjs/Observable';

@Component({
  // selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  pageTitle = 'Product Detail';
  product: IProduct;
  errorMessage: string;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _productService: ProductService
  ) {}

  ngOnInit() {
    let id = +this._route.snapshot.paramMap.get('id');
    this._productService.getProducts().subscribe(products => {
      this.product = products.find(p => p.productId === id);
    }, error => (this.errorMessage = <any>error));
  }
  onBack() {
    this._router.navigate(['/products']);
  }
}
