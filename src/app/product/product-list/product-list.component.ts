import { Component, OnInit} from '@angular/core';
import { IProduct } from '../product';
import { ProductService } from '../services';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  pageTitle = 'Product List';
  showImage = true;
  _listFilter: string;
  products: IProduct[];
  filteredProducts: IProduct[];
  errorMessage: string;
  get listFilter(): string {
    return this._listFilter;
  }
  set listFilter(value: string) {
    this._listFilter = value;
    this.filteredProducts = this._listFilter
      ? this.products.filter(
          p => p.productName.toLowerCase().indexOf(this._listFilter) !== -1
        )
      : this.products;
  }
  constructor(private _productService: ProductService) {}

  ngOnInit() {
    this._listFilter = '';
    this._productService.getProducts().subscribe(
      products => {
        this.products = products;
        this.filteredProducts = this.products;
      },
      error => this.errorMessage = <any>error
    );
  }
  toggleImage(): void {
    this.showImage = !this.showImage;
  }

  onRatingClicked(message: string): void {
    console.log(message);
  }
}
