import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StarComponent } from './star';
import { FormsModule } from '@angular/forms';
import { ConvertToSpacePipe } from './pipes';

@NgModule({
  imports: [CommonModule],
  declarations: [StarComponent, ConvertToSpacePipe],
  exports: [
    StarComponent,
    ConvertToSpacePipe,
    CommonModule,
    FormsModule
  ]
})
export class SharedModule {}
