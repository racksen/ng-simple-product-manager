import { Component, OnInit, OnChanges, SimpleChanges, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-star',
  templateUrl: './star.component.html',
  styleUrls: ['./star.component.css']
})
export class StarComponent implements OnChanges {
  @Input() rating: number;
  starPercentage: number;
  @Output() ratingClicked: EventEmitter<string> = new EventEmitter<string>();
  constructor() {}

  ngOnChanges(changes: SimpleChanges): void {
    // throw new Error('Method not implemented.');
    this.starPercentage = this.rating * 100 / 5;
  }
  onClick(): void {
    this.ratingClicked.emit(`${this.rating} was clicked`);

  }
}
