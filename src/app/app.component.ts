import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { RouterLinkActive } from '@angular/router';

@Component({
  selector: 'app-root',
  template: `
    <div class='container'>
        <nav class="navbar navbar-expand-lg navbar-light bg-default">
            <a class="navbar-brand" href="#">{{pageTitle}}</a>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
              <div class="navbar-nav">
                <a
                  [routerLinkActive]="['nav-item','nav-link','active']"
                  [routerLink]="['/home']">
                    Home
                </a>
                <a [routerLinkActive]="['nav-item','nav-link','active']"
                  [routerLink]="['/products']">Products</a>
              </div>
            </div>
          </nav>
          <div class='container'>
            <router-outlet></router-outlet>
        </div>
     </div>
    `,
  styles: []
})
export class AppComponent {
  pageTitle = "Simple Product Manager";
  public constructor(private titleService: Title) {
    this.titleService.setTitle(this.pageTitle);
  }
}
